import math

import chess

import rate
import utils


def negaMax(board, depth, alpha, beta, player, moves):
   



    if depth == 0 or len(list(board.legal_moves)) == 0: 
        score = rate.rate(board,  player, moves,alpha, beta)
        #f = open("moves.txt", "a")
        #listToStr = ' '.join([str(elem) for elem in moves]) 
        #f.write(listToStr)
        #f.write(str(score))
        #f.write("\n")
        #f.close()
        return (None, score)

    bestScore = -math.inf
    bestMove = list(board.legal_moves)[0]
    sortedmoves = utils.sorted_legal_moves(board)
    for move in sortedmoves:
        board.push(move)
        moves.append(move)
        new_move_with_score = negaMax(board, depth-1 , -beta, -alpha, not player, moves)
        new_move_with_score = (new_move_with_score[0], -new_move_with_score[1])
        board.pop()
        moves.pop()
        if new_move_with_score[1] > bestScore:
            bestScore = new_move_with_score[1]
            bestMove = move
        alpha = max(alpha, bestScore)
        if alpha >= beta:
            break


    return (bestMove, bestScore)
