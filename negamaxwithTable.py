import math

import chess

import rate
import utils

table = {}

class ttEntryClass:
    value = None
    depth = None
    flag = None
    
def transpositionTableLookup(board):
    result = None
    try:
        result = table[board.fen()]
    except KeyError:
        pass 
    return result



def transpositionTableStore(board, ttEntry):
    table[board.fen()] = ttEntry

def negaMax(board, depth, alpha, beta, player, moves):
    alphaOrig = alpha

    ttEntry = transpositionTableLookup(board)
    if ttEntry != None and ttEntry.depth >=depth:
        if ttEntry.flag == "EXACT":
            return (None, ttEntry.value)
        elif ttEntry.flag == "LOWERBOUND":
             alpha = max(alpha, ttEntry.value)
        elif ttEntry.flag == "UPPERBOUND":
            beta = min(beta, ttEntry.value)
        if alpha >= beta:
            return (None, ttEntry.value)





    if depth == 0 or len(list(board.legal_moves)) == 0: 
        score = rate.rate(board,  player, moves, alpha, beta)
        #f = open("moves.txt", "a")
        #listToStr = ' '.join([str(elem) for elem in moves]) 
        #f.write(listToStr)
        #f.write(str(score))
        #f.write("\n")
        #f.close()
        return (None, score)

    bestScore = -math.inf
    bestMove = list(board.legal_moves)[0]
    sortedmoves = utils.sorted_legal_moves(board)
    for move in sortedmoves:
        board.push(move)
        moves.append(move)
        new_move_with_score = negaMax(board, depth-1 , -beta, -alpha, not player, moves)
        new_move_with_score = (new_move_with_score[0], -new_move_with_score[1])
        board.pop()
        moves.pop()
        if new_move_with_score[1] > bestScore:
            bestScore = new_move_with_score[1]
            bestMove = move
        alpha = max(alpha, bestScore)
        if alpha >= beta:
            break

    
    ttEntry = ttEntryClass()
    ttEntry.value = bestScore
    if bestScore <= alphaOrig:
        ttEntry.flag = "UPPERBOUND"
    elif bestScore >= beta:
        ttEntry.flag = "LOWERBOUND"
    else:
        ttEntry.flag = "EXACT"
    ttEntry.depth = depth	
    transpositionTableStore(board, ttEntry)

    return (bestMove, bestScore)


