import chess


def rate(board, player, moves, alpha, beta ):
    return calScore(board, player, moves)
    stand_pat = calScore(board, player, moves)
    if( stand_pat >= beta ):
        return beta
    if( alpha < stand_pat ):
        alpha = stand_pat

    for move in board.legal_moves:
        if board.is_capture(move):
            board.push(move)        
            score = -rate(board, player, moves, -beta, -alpha )
            board.pop()

            if( score >= beta ):
                return beta
            if( score > alpha ):
                alpha = score  
    return alpha




def calScore(board, player, moves): # True: white False: black

    score = 0
    if board.is_checkmate():
        return -(10000000 - len(moves))
    if board.is_stalemate():
        return 0


    for col in range(0,8):
      numofpawnColw = 0
      numofpawnColb = 0
      for row in range(0,8):
        squareIndex=row*8+col
        square=chess.SQUARES[squareIndex]
        piece = board.piece_at(square)
        if piece is not None:
            if piece.color == player:
                if piece.piece_type == 1:
                    score = score + 100 - numofpawnColw * 20
                    numofpawnColw += 1
                    if(player):
                        score = score + chess.square_rank(square) 
                    else:
                        score = score + (7-chess.square_rank(square)) 
                elif piece.piece_type == 2:
                    score = score + 300
                elif piece.piece_type == 3:
                    score = score + 300
                elif piece.piece_type == 4:
                    score = score + 500
                elif piece.piece_type == 5:
                    score = score + 900
            else:
                if piece.piece_type == 1:
                    score = score - 100 + numofpawnColb * 20
                    numofpawnColb += 1
                    if(player):
                        score = score - (7-chess.square_rank(square)) 
                    else:
                        score = score - chess.square_rank(square) 
                elif piece.piece_type == 2:
                    score = score - 300
                elif piece.piece_type == 3:
                    score = score - 300
                elif piece.piece_type == 4:
                    score = score - 500
                elif piece.piece_type == 5:
                    score = score - 900
    return score
    
    
