import chess


def replace_with_unicode(board_str):
    mapping = [

        ('r', '♖'),
        ('n', '♘'),
        ('b', '♗'),
        ('q', '♕'),
        ('k', '♔'),
        ('p', '♙'),
        ('R', '♜'),
        ('N', '♞'),
        ('B', '♝'),
        ('Q', '♛'),
        ('K', '♚'),
        ('P', '♟'),
    ]

    for k, v in mapping:
        board_str = board_str.replace(k, v)

    return board_str


def sorted_legal_moves(board: chess.Board):
    # Seeing best moves early speeds up pruning.

    to_key = lambda move: (
        board.is_capture(move),
        not board.is_attacked_by(not board.turn, move.to_square),
        len(board.attacks(move.to_square)) > 0,
    )

    # We want Trues to come first. But False < True. So negate.
    key = lambda move: tuple([not e for e in to_key(move)])

    return sorted(board.legal_moves, key=key)
