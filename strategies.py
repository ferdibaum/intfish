import math
import random
import time

import chess
import chess.polyglot

import utils
from negamax import negaMax
from rate import calScore, rate


def random_strategy(board):
    move = random.choice(list(board.legal_moves))
    return move.uci()


GLOBAL_DEPTH = 4


def intfish(board):
    move = None
    if(board.fullmove_number < 1500):
        with chess.polyglot.open_reader("Eman.bin") as reader:
            try:
                bookentry = next(reader.find_all(board))
                move = bookentry.move()
            except StopIteration:
                pass
            
    if move == None:
        print("Computer Move")
        move_with_score = negaMax(board, GLOBAL_DEPTH, -math.inf, math.inf, board.turn, [])
        move = move_with_score[0]
    else:
        print("Boook Move")
    return move



def play(board):
    print(board.turn)
    print(calScore(board, True, []))
    print(rate(board, True, [],-math.inf, math.inf))
    print(calScore(board, False, []))
    print(rate(board, False, [],-math.inf, math.inf))
    print(utils.replace_with_unicode(str(board)))
    while not board.is_checkmate():
        userInput= input("Player or Computer Move? (P/C)")
        while userInput not in ["P", "C"]:
            userInput= input("Player or Computer Move? (P/C)")
        if userInput == "P":
            print(board.legal_moves)
            userInput= input("Welchen Move?")
            move = chess.Move.from_uci(userInput)
            while move not in board.legal_moves:
                userInput= input("Welchen Move?")
            board.push(move)
        elif userInput == "C":
            
            tic = time.perf_counter()
            move = None
           
            with chess.polyglot.open_reader("Eman.bin") as reader:
                try:
                    bookentry = next(reader.find_all(board))
                    move = bookentry.move()
                except StopIteration:
                    pass
                    
            if move == None:
                print("Computer Move")
                move_with_score = negaMax(board, GLOBAL_DEPTH, -math.inf, math.inf, board.turn, [])
                print(move_with_score[1])
                move = move_with_score[0]
            else:
                print("Boook Move")
            board.push(move)
            print(move)
            toc = time.perf_counter()
            print(f"Took {toc - tic:0.4f} seconds")
        print(utils.replace_with_unicode(str(board)))

board = chess.Board()

#board.set_fen("rnb1kb2/2q1ppp1/2p2n1r/1p1p4/p2P1P1p/2PBPN2/PP1K2PP/RNBQ3R b q - 3 11") 
#play(board)


#print(intfish(board))
# Add your strategies to this dictionary.
# Key: name
# Value: function from chess.Board to chess.Move
strategies = {'random':random_strategy, 'intfish':intfish}
